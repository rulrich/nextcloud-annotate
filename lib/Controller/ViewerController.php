<?php
declare(strict_types=1);
namespace OCA\PDFAnnotate\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\ContentSecurityPolicy;
use OCP\AppFramework\Http\DataDisplayResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\RedirectResponse;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\Constants;
use OCP\Files\Folder;
use OCP\Files\IRootFolder;
use OCP\Files\NotFoundException;
use OCP\IRequest;
use OCP\ISession;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\IUserManager;
use OCP\IUserSession;
use OCP\Share\Exceptions\ShareNotFound;
use OCP\Share\IManager;

class ViewerController extends Controller {

	/** @var string */
	private $userId;

	/** @var IURLGenerator */
	private $urlGenerator;

	/** @var IRootFolder */
	private $root;

	/** @var IUserManager */
	private $userManager;

	/** @var IUserSession */
	private $userSession;

	/**
	 * @param string $AppName
	 * @param IRequest $request
	 * @param $userId
	 * @param IRootFolder $root
	 * @param IURLGenerator $urlGenerator
	 * @param IUserManager $userManager
	 * @param IUserSession $userSession
	 */
	public function __construct(
			string $AppName,
			IRequest $request,
			$UserId,
			IRootFolder $root,
			IURLGenerator $urlGenerator,
			IUserManager $userManager,
			IUserSession $userSession
			) {
		parent::__construct($AppName, $request);
		$this->userId = $UserId;
		$this->root = $root;
		$this->urlGenerator = $urlGenerator;
		$this->userManager = $userManager;
		$this->userSession = $userSession;
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @return TemplateResponse|RedirectResponse
	 */
	public function index($fileId) {

		$fileUrl = $this->urlGenerator->getBaseUrl() . '/index.php/apps/pdfannotate/d/' . $fileId;
		$url = $this->urlGenerator->getBaseUrl() . '/index.php/apps/pdfannotate/viewer/' . $fileId . '?file=' . $fileUrl;
		$params = [
			'url' => $url,
		];
		$response = new TemplateResponse($this->appName, 'index', $params);
		$policy = new ContentSecurityPolicy();
		$policy->addAllowedFrameDomain('\'self\'');
		$response->setContentSecurityPolicy($policy);
		return $response;
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @return TemplateResponse|array
	 */
	public function viewer(string $fileId) {
		$permissions = $files = $this->root->getUserFolder($this->userId)->getById($fileId)[0]->getPermissions();
		$currentUser = $this->userManager->get($this->userId);
		$displayName= "";
		if ($currentUser instanceof IUser) {
			$displayName = $currentUser->getDisplayName();
		}
		$params = [
			'urlGenerator' => $this->urlGenerator,
			'fileId' => $fileId,
			'userId' => $this->userId,
			'displayName' => $displayName,
			'permissions' => $permissions,
		];
		$response = new TemplateResponse($this->appName, 'viewer', $params, 'blank');
		$policy = new ContentSecurityPolicy();
		$policy->allowEvalScript(true);  // Required for paper.js
		$policy->addAllowedWorkerSrcDomain("'self'");
		$response->setContentSecurityPolicy($policy);
		return $response;
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @return DataDisplayResponse
	 */
	public function downloadFile(string $fileId) {
		$userid = $this->userSession->getUser()->getUID();

		$file = $this->root->getUserFolder($userid)->getById($fileId)[0];
		if (!$file instanceof \OCP\Files\File) return new DataResponse([], Http::STATUS_NOT_FOUND);

		try {
			$content = $file->getContent();
		} catch(\OCP\Files\NotFoundException $e) {
			return new DataResponse([], Http::STATUS_NOT_FOUND);
		}

		return new DataDisplayResponse($content, Http::STATUS_OK, [
			'Content-Type' => $file->getMimeType(),
		]);
	}
}
